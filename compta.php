<?php

require_once "defaultincludes.inc";

print_header($day, $month, $year, '', '');

echo "<a href=\"compta/export_csv.php\">Télécharger</a>";

$requete = "SELECT num_orga, orga FROM $tbl_tarif ";
$sql = sql_query($requete);
if(mysqli_num_rows($sql) > 0)
{
    $i = 0;
    echo"<fieldset><legend>Modifier la tarification d'une salle :</legend>";
    echo"<form method=POST action='compta/modif_tarif.php'>";
    echo "Selectionnez une salle : ";
    echo "<select name=orga>";
    while($Row = mysqli_fetch_assoc($sql))
    {
        
        foreach($Row as $clef => $valeur){
            if ($i%2==0){    
                $i++;
                echo "<option value=".$valeur.">";
            }
            else{
                $i--;
                echo $valeur."</option>";
            }
        }
    }
    echo"</select></br>";
}
echo "Entrez la nouvelle tarification : <input type=number name=tarif required></br>";
echo "<input type=submit value=Envoyer>";
echo "</form></fieldset>";